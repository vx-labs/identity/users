package resources

import (
	"github.com/vx-labs/identity-types/server"
	"github.com/vx-labs/identity-api/store"
	"github.com/vx-labs/go-rest-api"
	"encoding/json"
	"fmt"
	"github.com/vx-labs/users/types"
	"github.com/vx-labs/identity-api/index"
	"github.com/vx-labs/identity-api/filter"
)

type userHandler struct {
	state server.StateStore
	events chan server.Event
	commands chan server.Event
}

func UserHandler() server.ResourceHandler {
	u := &userHandler{
		events: make(chan server.Event),
		commands: make(chan server.Event),
	}
	u.state = store.NewMemoryStore(u.Indexes())
	return u
}

func (h *userHandler) Store() server.StateStore {
	return h.state
}
func (h *userHandler) Schema() server.Resource {
	return &types.User{}
}
func (h *userHandler) Indexes() map[string]server.ResourceIndex {
	return index.IdOnly().Add(server.ResourceIndex{
		Id:     "google-id",
		Unique: true,
		Field:  "GoogleId",
	}).Add(server.ResourceIndex{
		Id:     "name",
		Unique: false,
		Field:  "Name",
	}).Add(server.ResourceIndex{
		Id:     "surname",
		Unique: false,
		Field:  "Surname",
	}).Add(server.ResourceIndex{
		Id:     "email",
		Unique: true,
		Field:  "Email",
	}).Build()
}
func (h *userHandler) EventsSubscriptions() map[string]server.EventHandler {
	return map[string]server.EventHandler{}
}
func (h *userHandler) CommandsSubscriptions() map[string]server.EventHandler {
	return map[string]server.EventHandler{}
}
func (h *userHandler) Events() chan server.Event{
	return h.events
}
func (h *userHandler) Commands() chan server.Event{
	return h.commands
}

func (h *userHandler) Grammar() api.Grammar {
	return api.Grammar{
		Plural:   "users",
		Singular: "user",
	}
}

func (h *userHandler) Created(b []byte) error {
	u := types.User{}
	err := json.Unmarshal(b, &u)
	if err != nil {
		return err
	}
	if u.Validate() {
		return h.state.Insert(&u)
	}
	return fmt.Errorf("invalid payload provided")
}

func (h *userHandler) Updated(id, owner string, b []byte) error {
	u, err := h.state.GetOne("id", id, filter.NewResourceFilter().Build())
	if err != nil {
		return err
	}
	err = json.Unmarshal(b, u)
	if err == nil && u.Validate() {
		return h.state.Insert(u)
	} else {
		return err
	}
}
func (h *userHandler) Deleted(id string, owner string) error {
	u, err := h.state.GetOne("id", id, filter.NewResourceFilter().Build())
	if err != nil {
		return err
	}
	return h.state.Delete(u)
}
