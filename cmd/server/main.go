package main

import (
	"context"
	"fmt"
	api "github.com/vx-labs/go-rest-api/client"
	authAPI "github.com/vx-labs/identity-api/authentication"
	"os"
	"github.com/vx-labs/users/resources"
)

func main() {
	fmt.Println("starting users service")

	logger := api.NewLogger()
	ctx := context.Background()
	auth, err := authAPI.NewClient(ctx, logger.WithField("source", "identityClient"))
	if err != nil {
		logger.Errorf("could not connect to authorization service: %s", err.Error())
		os.Exit(1)
	}
	server, err := auth.NewResourceServer(ctx, logger.WithField("source", "server"), "identity", "v1")
	if err != nil {
		logger.Errorf("could not connect spawn server: %s", err.Error())
		os.Exit(1)
	}

	server.AddResource("/", resources.UserHandler())
	fmt.Println(server.ListenAndServe(ctx, 8008))
}
