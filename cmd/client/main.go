package main

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	clientAPI "github.com/vx-labs/go-rest-api/client"
	authentication "github.com/vx-labs/identity-api/authentication"
	users "github.com/vx-labs/users/api"
	"github.com/vx-labs/users/types"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

func boot(role, secret, endpoint string) {
	if role != "" {
		os.Setenv("APPROLE_ID", role)
	}
	if secret != "" {
		os.Setenv("APPROLE_SECRET", secret)
	}
	if endpoint != "" {
		os.Setenv("AUTHORIZATION_ENDPOINT", endpoint)
	}
}

func main() {
	ctx := context.Background()
	logger := clientAPI.NewLogger()

	app := kingpin.New("users", "vx users api client")
	role := app.Flag("role", "identity API role").String()
	secret := app.Flag("secret", "identity API secret").String()
	endpoint := app.Flag("endpoint", "users API endpoint").Default("https://users.cloud.vx-labs.net/v1").String()

	appUsers := app.Command("users", "users management")
	appUsersList := appUsers.Command("list", "list users").Alias("ls")
	appUsersListFull := appUsersList.Flag("full", "include details").Default("false").Short('f').Bool()
	appUsersDel := appUsers.Command("rm", "delete user").Alias("delete")
	appUsersDelId := appUsersDel.Flag("instance", "user to delete").Short('i').String()

	appUsersCreate := appUsers.Command("create", "create user").Alias("new")
	appUsersCreateName := appUsersCreate.Flag("name", "user name").Short('n').String()
	appUsersCreatePol := appUsersCreate.Flag("policies", "user policies").Short('p').Strings()
	appUsersCreateGID := appUsersCreate.Flag("google-id", "user google id").Short('g').String()

	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))
	boot(*role, *secret, *endpoint)
	auth, err := authentication.NewClient(ctx, logger.WithField("source", "authentication_client"))
	if err != nil {
		logger.Fatal(err.Error())
	}
	a, err := users.NewClient(ctx, logger.WithField("source", "api_client"))
	if err != nil {
		logger.Fatal(err.Error())
	}
	a.WithJwtProvider(auth)

	switch cmd {
	case appUsersList.FullCommand():
		polList(ctx, logger, a, *appUsersListFull)
	case appUsersDel.FullCommand():
		a.Users().Delete(ctx, *appUsersDelId)
	case appUsersCreate.FullCommand():
		u := types.User{
			Name:     *appUsersCreateName,
			Policies: *appUsersCreatePol,
			GoogleId: *appUsersCreateGID,
		}
		a.Users().Create(ctx, u)
	}
}

func polList(ctx context.Context, logger *logrus.Logger, a users.Client, full bool) {
	l, err := a.Users().List(ctx)
	if err != nil {
		logger.Fatal(err.Error())
	}
	fmt.Printf("Users (%d):\n", len(l))
	for _, pol := range l {
		fmt.Printf("\n")
		fmt.Printf("  * %s\n", pol)
		if full {
			details, err := a.Users().ById(ctx, pol)
			if err == nil {
				fmt.Printf("    Name: %s\n", details.Name)
				fmt.Printf("    Google ID: %s\n", details.GoogleId)
				fmt.Printf("    Surname: %s\n", details.Surname)
				fmt.Println("    Policies:")
				for _, pol := range details.Policies {
					fmt.Printf("      * %s\n", pol)
				}
			}
		}
	}
}
