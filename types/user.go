package types

import (
	"github.com/vx-labs/identity-types/server"
	"time"
	"encoding/json"
)

type User struct {
	Id       string          `json:"id"`
	GoogleId string          `json:"google_id"`
	Policies []string        `json:"policies"`
	Surname  string          `json:"surname"`
	Name     string          `json:"name"`
	Picture  string          `json:"picture"`
	Email    string          `json:"email"`
	Scopes   map[string]bool `json:"scopes"`
	Updated  time.Time       `json:"updated"`
	Created  time.Time       `json:"created"`
}

func (u *User) UniqueId() string {
	return u.Id
}
func (u *User) Kind() string {
	return "User"
}
func (u *User) AuthorizationPolicies() []string {
	return u.Policies
}

func (h *User) OwnerId() string {
	return "_root"
}
func (h *User) Scope() map[string]bool {
	return h.Scopes
}
func (h *User) WithOwner(owner string) server.Resource {
	return h
}

func (h *User) WithId(id string) server.Resource {
	c := *h
	c.Id = id
	return &c
}

func (h *User) WithUpdated(t time.Time) server.Resource {
	c := *h
	c.Updated = t
	return &c
}
func (h *User) WithCreated(t time.Time) server.Resource {
	c := *h
	c.Created = t
	return &c
}

func (h *User) Validate() bool {
	return h.Name != "" && h.Surname != "" && h.Email != "" && h.GoogleId != ""
}
func (h *User) FromJSON(b []byte) error {
	return json.Unmarshal(b, &h)
}
func (h *User) ToJSON() ([]byte, error) {
	return json.Marshal(h)
}
