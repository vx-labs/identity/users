package api

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/vx-labs/go-rest-api"
	"github.com/vx-labs/go-rest-api/opts"
	"github.com/vx-labs/users/types"
)

type UsersClient interface {
	Create(ctx context.Context, user types.User) (types.User, error)
	Update(ctx context.Context, user types.User) (types.User, error)
	List(ctx context.Context) ([]string, error)
	ById(ctx context.Context, id string) (types.User, error)
	ByGoogleId(ctx context.Context, id string) (types.User, error)
	Delete(ctx context.Context, id string) error
}

type usersClient struct {
	api    api.Client
	logger *logrus.Entry
}

func newUsersClient(api api.Client, logger *logrus.Entry) UsersClient {
	return &usersClient{
		api:    api,
		logger: logger,
	}
}

func (c *usersClient) Delete(ctx context.Context, id string) error {
	req := c.api.Delete("users", id)
	return c.api.Do(ctx, req)
}
func (c *usersClient) Create(ctx context.Context, user types.User) (types.User, error) {
	resp := map[string]types.User{}
	req := c.api.Create("users", user, &resp)
	err := c.api.Do(ctx, req)
	if err != nil {
		c.logger.Errorf(err.Error())
		return types.User{}, err
	}
	return resp["user"], nil
}
func (c *usersClient) Update(ctx context.Context, user types.User) (types.User, error) {
	resp := map[string]types.User{}
	req := c.api.Update("users", user.Id, user, &resp)
	err := c.api.Do(ctx, req)
	if err != nil {
		c.logger.Errorf(err.Error())
		return types.User{}, err
	}
	return resp["user"], nil
}
func (c *usersClient) ById(ctx context.Context, id string) (types.User, error) {
	resp := map[string]types.User{}
	req := c.api.Read("users", id, &resp)
	err := c.api.Do(ctx, req)
	if err != nil {
		c.logger.Errorf(err.Error())
		return types.User{}, err
	}
	return resp["user"], nil
}
func (c *usersClient) ByGoogleId(ctx context.Context, id string) (types.User, error) {
	resp := map[string]types.User{}
	req := c.api.Read("users", id, &resp).WithOptions(opts.Values{
		"index": {"google_id"},
	})
	err := c.api.Do(ctx, req)
	if err != nil {
		c.logger.Errorf(err.Error())
		return types.User{}, err
	}
	return resp["user"], nil
}
func (c *usersClient) List(ctx context.Context) ([]string, error) {
	resp := map[string][]string{}
	req := c.api.List("users", &resp)
	err := c.api.Do(ctx, req)
	if err != nil {
		c.logger.Errorf(err.Error())
		return nil, err
	}
	return resp["users"], nil
}
